package java8InAction;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import static java.util.Comparator.comparing;

public class PrintApples {

	public static void main(String[] args) {
		List<Apple> apples = Arrays.asList(new Apple(NamedColor.RED, 80),
									       new Apple(90),
										   new Apple(NamedColor.GREEN, 120),
										   new Apple(NamedColor.RED, 70),
										   new Apple(NamedColor.RED),
										   new Apple(),
										   new Apple(130));
		
		prettyPrintApple(apples, (apple) -> "Color is: " + apple.getColor());	
		System.out.println();
		prettyPrintApple(apples, (apple) -> "Weight is: " + apple.getWeight() + " grams");
		System.out.println();
		prettyPrintApple(apples, (apple) -> (apple.getWeight() >= 110 ? "Apple is heavy." : "Apple is light."));
		
		System.out.println();
		apples.sort(comparing(Apple::getColor).thenComparing(Apple::getWeight));
		System.out.println(apples);
	}
	
	public static void prettyPrintApple(List<Apple> inventory, Function<Apple, String> descriptor) {
		for (Apple apple : inventory) {
			System.out.println(descriptor.apply(apple));
		}
	}
}