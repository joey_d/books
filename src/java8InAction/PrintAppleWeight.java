package java8InAction;

/*
 * Deprecated
 */
public class PrintAppleWeight implements AppleMessage {
	public static final PrintAppleWeight INSTANCE = new PrintAppleWeight();
	private PrintAppleWeight(){}
	
	public String message(Apple apple) {
		return "Weight is: " + apple.getWeight() + " grams";
	}
}
