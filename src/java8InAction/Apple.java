package java8InAction;

public class Apple {
	
	private final NamedColor color;
	private final int weight;
	
	public static final int WEIGHT_DEFAULT = 100;
	public static final NamedColor COLOR_DEFAULT = NamedColor.GREEN;
	
	public Apple() {
		this(COLOR_DEFAULT, WEIGHT_DEFAULT);
	}
	
	public Apple(NamedColor color) {
		this(color, WEIGHT_DEFAULT);
	}
	
	public Apple(int weight) {
		this(COLOR_DEFAULT, weight);
	}
	
	public Apple(NamedColor color, int weight) {
		this.color = color;
		this.weight = weight;
	}
	
	public String getColor() {
		return this.color.getColorName();
	}
	
	public int getWeight() {
		return this.weight;
	}
	
	@Override public String toString() {
		return this.getWeight() + " gram " + this.getColor() + " apple";
	}
}
