package java8InAction;

import java.awt.Color;

public enum NamedColor {
	BLUE(Color.BLUE, "Blue"),
	RED(Color.RED, "Red"),
	GREEN(Color.GREEN, "Green");
	
	private final Color awtColor;
	private final String colorName;
	
	private NamedColor(Color awtColor, String name) {
		this.awtColor = awtColor;
		this.colorName = name;
	}
	
	public Color getAwtColor() {
		return this.awtColor;
	}
	
	public String getColorName() {
		return this.colorName;
	}
}
