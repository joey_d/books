package java8InAction;
/*
 * Deprecated
 */
public class PrintAppleColor implements AppleMessage {
	public static final PrintAppleColor INSTANCE = new PrintAppleColor();
	private PrintAppleColor(){};
	
	public String message(Apple apple) {
		return "Color is: " + apple.getColor();
	}
}
