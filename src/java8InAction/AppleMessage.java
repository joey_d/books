package java8InAction;

/*
 * Deprecated
 */
@FunctionalInterface public interface AppleMessage {
	String message(Apple apple);
}
